# soal-shift-sisop-modul-3-D09-2022
## Anggota Kelompok
|NRP|Nama|Soal|
|---|----|---|
|5025201085|Renita Caithleen Davita|Tidak Mengerjakan|
|5025201257|Sastiara maulikh|Tidak Mengerjakan|
|5025201140|Ezekiel Mashal Wicaksono|Soal 3|

## Soal3
* __soal3.c__
 
    Dalam code saya , saya dibagian awal membuat dulu directory awal yang nantinya akan digunakan sebagai tempat working space . Metode yang digunakan adalah `mkdir()` . `0755` merupakan permission code untuk pembuatan dir baru

    ```c
    void dirhartakarun(){
        int ret = 0;
        char dirName [10]= "hartakarun";
        ret = mkdir(dirName, 0755);
    }
    ```
    Kemudian untuk unzip , karena dibolehkan tidak menggunakan bahasa c , maka saya menggunakan function dari `bin` . Kemudian dipanggil menggunakan `execv`. Directory disesuaikan dengan workspace yaitu `/home/user/shift3/hartakarun`.
    
    ```c
    void unzip(){
        char *arg[] = {"unzip","/home/ezekielmw/shift3/hartakarun.zip","-d","/home/ezekielmw/shift3", NULL};
        execv("/usr/bin/unzip",arg);
    }
    ```
    Untuk mengecek file yang belum teralokasi , saya membuat `void existfile()`. Dalam function ini saya menggunakan `int exist` sebagai counter untuk file yang belum terambil yang di pointer ke memory menggunakan `buff` . Jika `exist` bernilai 0 maka akan di return 1 , sedangkan jika masih ada akan di return 0.
    ```c
    int existfile(const char *filename)
    {
        struct stat buff;
        int exist = stat(filename, &buff);
        if (exist == 0)
            return 1;
        else
            return 0;
    }
    ```
    Untuk func `void move()` digunakan untuk memindahkan file kedalam folder yang akan dibuat sesuai datatype yang ada.

    ```c
    void *move(void *filename)
    {
        char cwd[PATH_MAX];
        char directoryname[50], hide[100], hidename[100], file[100], ex_file[100];
        int i;
        strcpy(ex_file, filename);
        strcpy(hidename, filename);
        char *nama = strrchr(hidename, '/');
        strcpy(hide, nama);
        if (hide[1] == '.')
        {
            strcpy(directoryname, "hide");
        }
        else if (strstr(filename, ".") != NULL)
        {
            strcpy(file, filename);
            strtok(file, ".");
            char *token = strtok(NULL, "");
            for (i = 0; token[i]; i++)
            {
                token[i] = tolower(token[i]);
            }
            strcpy(directoryname, token);
        }
        else
        {
            strcpy(directoryname, "Unknown");
        }
        int exist = cfileexists(ex_file);
        if (exist)
            mkdir(directoryname, 0755);
        if (getcwd(cwd, sizeof(cwd)) != NULL)
        {
            char *nama = strrchr(filename, '/');
            char file_name[200];
            strcpy(file_name, cwd);
            strcat(file_name, "/");
            strcat(file_name, directoryname);
            strcat(file_name, nama);
            rename(filename, file_name);
        }
    }
    ```

