#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <wait.h>
#include <pwd.h>

void dirhartakarun(){
    int ret = 0;
    char dirName [10]= "hartakarun";
    ret = mkdir(dirName, 0755);
}
void unzip(){
    char *arg[] = {"unzip","/home/ezekielmw/shift3/hartakarun.zip","-d","/home/ezekielmw/shift3", NULL};
    execv("/usr/bin/unzip",arg);
}
int existfile(const char *filename)
{
    struct stat buff;
    int exist = stat(filename, &buff);
    if (exist == 0)
        return 1;
    else
        return 0;
}
void *move(void *filename)
{
    char cwd[PATH_MAX];
    char directoryname[50], hide[100], hidename[100], file[100], ex_file[100];
    int i;
    strcpy(ex_file, filename);
    strcpy(hidename, filename);
    char *nama = strrchr(hidename, '/');
    strcpy(hide, nama);
    if (hide[1] == '.')
    {
        strcpy(directoryname, "hide");
    }
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(directoryname, token);
    }
    else
    {
        strcpy(directoryname, "Unknown");
    }
    int exist = cfileexists(ex_file);
    if (exist)
        mkdir(directoryname, 0755);
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, directoryname);
        strcat(namafile, nama);
        rename(filename, namafile);
    }
}
int main(int argc, char *argv[])
{
    char cwd[PATH_MAX];
    void dirhartakarun();
    void unzip();
    int existfile(const char *filename);
    void *move(void *filename);
}